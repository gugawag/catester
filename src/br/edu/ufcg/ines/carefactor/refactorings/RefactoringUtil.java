package br.edu.ufcg.ines.carefactor.refactorings;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.IPDOMManager;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.testplugin.CProjectHelper;
import org.eclipse.cdt.core.testplugin.FileManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;


/**
 * Classe utilit�ria para criar projeto tempor�rio no workspace do eclipse CDT.
 * Entre outras fun��es, acrescenta arquivos ao projeto.
 * @author Gustavo Wagner, gugawag@gmail.com
 */
public class RefactoringUtil {
	
	//NullProgressMonitor pois n�o precisamos de interface gr�fica.
    public NullProgressMonitor	monitor;
    public IWorkspace 			workspace;
    public IProject 				project;
    public ICProject				cproject;
    public FileManager 			fileManager;
	public boolean				indexDisabled= false;
	
	private static Logger logger = Logger.getLogger(RefactoringUtil.class);


	//criando um projeto no eclipse
	public void initProject(String nomeProjeto) {
		if (project != null) {
			return;
		}
        if (CCorePlugin.getDefault() != null && CCorePlugin.getDefault().getCoreModel() != null) {
			monitor = new NullProgressMonitor();
			
			workspace = ResourcesPlugin.getWorkspace();
			
	        try {
	            cproject = CProjectHelper.createCCProject(nomeProjeto, "bin", IPDOMManager.ID_NO_INDEXER); //$NON-NLS-1$ //$NON-NLS-2$
	        
	            project = cproject.getProject();
	            
	        } catch (CoreException e) {
	            /*boo*/
	        }
			if (project == null){
				logger.error("Projeto est� nulo");
			}				
	
			//Create file manager
			fileManager = new FileManager();
        }
	}
	
    public void cleanupProject() throws Exception {
        try{
	        project.delete(true, false, monitor);
	    } catch (Throwable e) {
	        /*boo*/
	    } finally {
	    	project= null;
	    }
    }
    
    //acrescenta arquivo ao projeto
    public IFile importFile(String fileName, String contents) throws Exception {
		// Obtain file handle
		IFile file = project.getProject().getFile(fileName);
		
		//s� gera arquivo se tiver conte�do. Se n�o, arquivo j� tem que existir
		if (contents != null){
			InputStream stream = new ByteArrayInputStream(contents.getBytes());
			// Create file input stream
			if (file.exists()) {
			    file.setContents(stream, false, false, monitor);
			} else {
				IPath path = file.getLocation();
				path = path.makeRelativeTo(project.getLocation());
				if (path.segmentCount() > 1) {
					path = path.removeLastSegments(1);
					
					for (int i = path.segmentCount() - 1; i >= 0; i--) {
						IPath currentPath = path.removeLastSegments(i);
						IFolder folder = project.getFolder(currentPath);
						if (!folder.exists()) {
							folder.create(false, true, null);
						}
					}
				}
				file.create(stream, false, monitor);	
			}
		}
		
		fileManager.addFile(file);
		
		return file;
	}
    
    public IProject getProject(){
    	return project;
    }


}
