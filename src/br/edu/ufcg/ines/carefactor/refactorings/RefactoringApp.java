package br.edu.ufcg.ines.carefactor.refactorings;

import java.util.Scanner;

import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.IPDOMManager;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.internal.core.CCoreInternals;
import org.eclipse.cdt.internal.core.pdom.PDOMManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

public class RefactoringApp {
	
	private String projectName = "ACExperiments";
	private String oldName = "GlobalVariableDeclaration_0";
	private String newName = "fDeuCerto";
	private String srcFolder = "src";
	private String programPrefix = "p";
	private String filePrefix = "f";
	private String refactoredFolder = "refactored";
	private String errorFolder = "errors";
	private String infoFolder = "info";
	
	private static final String DEFAULT_INDEXER_TIMEOUT_SEC = "10";
	private static final String INDEXER_TIMEOUT_PROPERTY = "indexer.timeout";
	protected static final int INDEXER_TIMEOUT_SEC =
			Integer.parseInt(System.getProperty(INDEXER_TIMEOUT_PROPERTY, DEFAULT_INDEXER_TIMEOUT_SEC));
	
	private RefactoringUtil ru;
	
	public RefactoringApp(){
		ru = new RefactoringUtil();
	}
	
	public void init(){
		ru.initProject(projectName);
	}
	
	private String fileName(int fileNumber) {
		return filePrefix + fileNumber + ".c";
	}
	
	public void runRenameVariableRefactoring(String oldName, String newName) throws Exception{
		//inicia ou gera o projeto
		init();
		
		//faz refactoring para todos os programas (arquivos) dentro da pasta src do projeto
		int programNumber = 1;
		String fileName = fileName(programNumber);
		String filePath = srcFolder + "/" + fileName;
		IFile cpp = ru.importFile(filePath, null );
		while (cpp.exists()){
	        Scanner sc = new Scanner(cpp.getContents());
	        StringBuffer sb = new StringBuffer();
	        while (sc.hasNextLine()){
	        	sb.append(sc.nextLine()+"\n");
	        }
	        sc.close();
	        int offset = sb.indexOf(oldName);
	        
	        try{
	        	RenameRefactor renRefactor = new RenameRefactor();
	        	RefactoringStatus statusPreConditions = renRefactor.checkConditions(cpp, offset, newName);
	        	if (statusPreConditions.hasError() || statusPreConditions.hasFatalError()){
	        		//TODO: colocar a real mensagem de erro
	        		String message = renRefactor.getRefactorMessages(cpp, offset, newName);
	        		cpp = ru.importFile(errorFolder + "/" + fileName, message );
	    	        fileName = programPrefix + ++programNumber + ".c";
	    			cpp = ru.importFile(srcFolder + "/" + fileName, null );
	        		continue;
	        	} else if (statusPreConditions.hasInfo() || statusPreConditions.hasWarning()){
	        		//TODO: colocar a real mensagem de info/warning
	        		String message = renRefactor.getRefactorMessages(cpp, offset, newName);

	        		cpp = ru.importFile(infoFolder + "/" + fileName, message );
	        	}
	        	cpp = ru.importFile(refactoredFolder + "/" + fileName, sb.toString() );
	        	
	        	RefactoringStatus statusRefactoring = renRefactor.checkConditionsAndPerformRefactoring(cpp, offset, newName);
	        	
	//        	IFile cppRefactored= RefactoringUtil.importFile("refactored/test_function.c", sb.toString() );
	        	//FIXME se executar o m�todo abaixo d� exce��o.
	//        	renRefactor.performRefactoring(cpp, offset, newName);
	        } catch(Exception e){
	        	System.out.println("Exce��o mesmo!");
	        	e.printStackTrace();
	        }
	        fileName = programPrefix + ++programNumber + ".c";
			cpp = ru.importFile(srcFolder + "/" + fileName, null );
		}

	}
	
	//extrai, por enquanto, n�mero 4
	public void runExtractLocalVariableRefactoring() throws Exception{
	}
	
	//extrai, por enquanto, n�mero 2
	public void runExtractConstantRefactoring() throws Exception{
		//inicia ou gera o projeto
		init();
		
		//faz refactoring para todos os programas (arquivos) dentro da pasta src do projeto
		int programNumber = 1;
		String fileName = fileName(programNumber);
		IFile cpp = ru.importFile(srcFolder + "/" + fileName, null );
		while (cpp.exists()){
	        Scanner sc = new Scanner(cpp.getContents());
	        StringBuffer sb = new StringBuffer();
	        while (sc.hasNextLine()){
	        	sb.append(sc.nextLine()+"\n");
	        }
	        sc.close();
	        int offset = sb.indexOf("4");
	        
	        try{
	        	newName = "EXTRACTED";
	        	ExtractConstantRefactor extractConstant = new ExtractConstantRefactor();
	        	RefactoringStatus statusPreConditions = extractConstant.checkConditions(cpp, offset, 1, newName, ru.cproject);
	        	if (statusPreConditions.hasError() || statusPreConditions.hasFatalError()){
	        		//TODO: colocar a real mensagem de erro
	        		String message = extractConstant.getRefactorMessages(cpp, offset, 1, newName, ru.cproject);
	        		cpp = ru.importFile(errorFolder + "/" + fileName, message );
	    	        fileName = programPrefix + ++programNumber + ".c";
	    			cpp = ru.importFile(srcFolder + "/" + fileName, null );
	        		continue;
	        	} else if (statusPreConditions.hasInfo() || statusPreConditions.hasWarning()){
	        		//TODO: colocar a real mensagem de info/warning
	        		String message = extractConstant.getRefactorMessages(cpp, offset, 1, newName, ru.cproject);

	        		cpp = ru.importFile(infoFolder + "/" + fileName, message );
	        	}
	        	cpp = ru.importFile(refactoredFolder + "/" + fileName, sb.toString() );
	        	
	        	RefactoringStatus statusRefactoring = extractConstant.checkConditionsAndPerformRefactoring(cpp, offset, 1, newName, ru.cproject);
	        	
	//        	IFile cppRefactored= RefactoringUtil.importFile("refactored/test_function.c", sb.toString() );
	        	//FIXME se executar o m�todo abaixo d� exce��o.
	//        	renRefactor.performRefactoring(cpp, offset, newName);
	        } catch(Exception e){
	        	System.out.println("Exce��o mesmo!");
	        	e.printStackTrace();
	        }
	        fileName = programPrefix + ++programNumber + ".c";
			cpp = ru.importFile(srcFolder + "/" + fileName, null );
		}

	}
	
	public void runExtractFunctionRefactoring() throws Exception{
		//inicia ou gera o projeto
		init();
		
		//faz refactoring para todos os programas (arquivos) dentro da pasta src do projeto
		int programNumber = 1;
		String fileName = fileName(programNumber);
		IFile cpp = ru.importFile(srcFolder + "/" + fileName, null );
		while (cpp.exists()){
	        Scanner sc = new Scanner(cpp.getContents());
	        StringBuffer sb = new StringBuffer();
	        while (sc.hasNextLine()){
	        	sb.append(sc.nextLine()+"\n");
	        }
	        sc.close();
	        int offset = sb.indexOf("int");
	        int offset2 = sb.indexOf(";");
	        
	        try{
	        	newName = "EXTRACTED";
	        	ExtractFunctionRefactor extractFunction = new ExtractFunctionRefactor();
	        	int length = offset2 - offset;
	        	System.out.println("Tamanho: " + length);
	        	RefactoringStatus statusPreConditions = extractFunction.checkConditions(cpp, offset, length, newName, ru.cproject);
	        	if (statusPreConditions.hasError() || statusPreConditions.hasFatalError()){
	        		//TODO: colocar a real mensagem de erro
	        		String message = extractFunction.getRefactorMessages(cpp, offset, length, newName, ru.cproject);
	        		cpp = ru.importFile(errorFolder + "/" + fileName, message );
	    	        fileName = programPrefix + ++programNumber + ".c";
	    			cpp = ru.importFile(srcFolder + "/" + fileName, null );
	        		continue;
	        	} else if (statusPreConditions.hasInfo() || statusPreConditions.hasWarning()){
	        		//TODO: colocar a real mensagem de info/warning
	        		String message = extractFunction.getRefactorMessages(cpp, offset, length, newName, ru.cproject);

	        		cpp = ru.importFile(infoFolder + "/" + fileName, message );
	        	}
	        	cpp = ru.importFile(refactoredFolder + "/" + fileName, sb.toString() );
	        	
	        	RefactoringStatus statusRefactoring = extractFunction.checkConditionsAndPerformRefactoring(cpp, offset, length, newName, ru.cproject);
	        	
	//        	IFile cppRefactored= RefactoringUtil.importFile("refactored/test_function.c", sb.toString() );
	        	//FIXME se executar o m�todo abaixo d� exce��o.
	//        	renRefactor.performRefactoring(cpp, offset, newName);
	        } catch(Exception e){
	        	System.out.println("Exce��o mesmo!");
	        	e.printStackTrace();
	        }
	        fileName = programPrefix + ++programNumber + ".c";
			cpp = ru.importFile(srcFolder + "/" + fileName, null );
		}

	}

}
