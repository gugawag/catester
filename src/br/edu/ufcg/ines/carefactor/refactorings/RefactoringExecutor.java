package br.edu.ufcg.ines.carefactor.refactorings;

import java.util.Scanner;

import org.eclipse.core.resources.IFile;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;

import br.edu.ufcg.ines.carefactor.experiments.Executor;

public class RefactoringExecutor {
	
	private String projectName = "ACExperiments";
	private String oldName = "GlobalVariableDeclaration_0";
	private String srcFolder = "src";
	private String programPrefix = "p";
	private String filePrefix = "f";
	
	private static final String DEFAULT_INDEXER_TIMEOUT_SEC = "10";
	private static final String INDEXER_TIMEOUT_PROPERTY = "indexer.timeout";
	protected static final int INDEXER_TIMEOUT_SEC =
			Integer.parseInt(System.getProperty(INDEXER_TIMEOUT_PROPERTY, DEFAULT_INDEXER_TIMEOUT_SEC));
	
	private RefactoringUtil ru;
	
	public RefactoringExecutor(){
		ru = new RefactoringUtil();
		ru.initProject(projectName);
	}
	
	private String fileName(int fileNumber) {
		return filePrefix + fileNumber + ".c";
	}

	/**
	 * 
	 * @param srcFolder
	 * @param programNumber
	 * @param fileName
	 * @param oldName
	 * @param newName
	 * @param refactoringFolder The folder where the refactored files will be stored
	 * @throws Exception
	 */
	public RefactoringStatus runRenameRefactoring(String srcFolder, int programNumber, String fileName, String oldName, String newName, String refactoringFolder) throws Exception{
		RefactoringStatus refactoringStatus = null;
		//faz refactoring para todos os programas (arquivos) dentro da pasta src do projeto
		IFile cpp = ru.importFile(srcFolder + "/" + programNumber + "/" + fileName, null );
	        Scanner sc = new Scanner(cpp.getContents());
	        StringBuffer sb = new StringBuffer();
//	        FileInputStream file 
	        while (sc.hasNextLine()){
	        	sb.append(sc.nextLine()+"\n");
	        }
	        sc.close();
	        int offset = sb.indexOf(oldName);
	        
	        try{
	        	RenameRefactor renRefactor = new RenameRefactor();
	        	refactoringStatus = renRefactor.checkConditions(cpp, offset, newName);
        		String message = renRefactor.getRefactorMessages(cpp, offset, newName);
        		String programRefactoredFolderPrefix = refactoringFolder + "/" + programNumber + "/" + oldName + "_" + newName + "/";
	        	if (refactoringStatus.hasError() || refactoringStatus.hasFatalError()){
	        		cpp = ru.importFile(refactoringFolder + "/" + programNumber + "/" + Executor.errorFolder + "/" + oldName + "_" + newName + "/" + fileName, message );
	        		return refactoringStatus;
	        	} else if (refactoringStatus.hasInfo() || refactoringStatus.hasWarning()){
	        		cpp = ru.importFile(refactoringFolder + "/" + programNumber + "/" + Executor.infoFolder + "/" + oldName + "_" + newName + "/" + fileName, message );
	        	} else{
	        		cpp = ru.importFile(programRefactoredFolderPrefix + fileName, sb.toString() );
	        	}
	        	
	        	refactoringStatus = renRefactor.checkConditionsAndPerformRefactoring(cpp, offset, newName);
	        	
	        } catch(Exception e){
	        	System.out.println("Exce��o mesmo!");
	        	e.printStackTrace();
	        	return refactoringStatus;
	        }
	        return refactoringStatus;

	}

}
