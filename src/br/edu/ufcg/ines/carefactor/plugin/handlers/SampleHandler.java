package br.edu.ufcg.ines.carefactor.plugin.handlers;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.eclipse.cdt.core.CCorePlugin;
import org.eclipse.cdt.core.dom.ast.IASTFileLocation;
import org.eclipse.cdt.core.dom.ast.IBinding;
import org.eclipse.cdt.core.dom.ast.IFunction;
import org.eclipse.cdt.core.index.IIndex;
import org.eclipse.cdt.core.index.IIndexBinding;
import org.eclipse.cdt.core.index.IIndexName;
import org.eclipse.cdt.core.index.IndexFilter;
import org.eclipse.cdt.core.model.CoreModel;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;

import br.edu.ufcg.ines.carefactor.app.App;
import br.edu.ufcg.ines.carefactor.experiments.Executor;
import br.edu.ufcg.ines.carefactor.experiments.statistics.RefactoringData;
import br.edu.ufcg.ines.carefactor.experiments.statistics.StatisticGenerator;
import br.edu.ufcg.ines.carefactor.util.ShowCDTStructures;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SampleHandler extends AbstractHandler {
	
	private static Logger logger = Logger.getLogger(SampleHandler.class);
	
	/**
	 * The constructor.
	 */
	public SampleHandler() {
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		//Configuring log4j
		try {
			App.configuraLog4j();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
//		ShowCDTStructures.main(null);
		
		
//		//rename function
//		Executor experimentExecutorRenameFunction = new Executor();
//		experimentExecutorRenameFunction.runRenameRefactoring(ICElement.C_FUNCTION);
//		
//		//printing statistics
//		StatisticGenerator stGen = experimentExecutorRenameFunction.stGen;
//		Map<String, RefactoringData> mapData = stGen.getInstance().getProgramExecutions();
//		logger.info("Program count: " + mapData.keySet().size());
//		Properties propertiesStatistics = new Properties();
//		for (String programNumber: mapData.keySet()){
//			RefactoringData refData = mapData.get(programNumber);
//			logger.info("Total refactoring execution time (s): " + refData.getTotalExecutionTimeInSeconds());
//			String resultAsString = refData.getResultsAsString();
//			logger.info(resultAsString);
//			propertiesStatistics.put(programNumber, resultAsString);
//		}
//		
//		String statisticFile = Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-localvar.properties";
//		try {
//			propertiesStatistics.store(new FileOutputStream(statisticFile),"");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
		//rename local variables
		Executor experimentExecutorLocalVariable = new Executor();
		StatisticGenerator.setNullInstance();
		experimentExecutorLocalVariable.runRenameRefactoring(ICElement.C_VARIABLE_LOCAL, "aa_0", "*aa_0");
		
		//printing statistics
		StatisticGenerator stGen = StatisticGenerator.getInstance();
		Map<String, RefactoringData> mapData = stGen.getProgramExecutions();
		logger.info("Program count: " + mapData.keySet().size());
//		Properties propertiesStatistics = new Properties();
//		for (String programNumber: mapData.keySet()){
//			RefactoringData refData = mapData.get(programNumber);
//			logger.info("Total refactoring execution time (s): " + refData.getTotalExecutionTimeInSeconds());
//			String resultAsString = refData.getResultsAsString();
//			logger.info(resultAsString);
//			propertiesStatistics.put(programNumber, resultAsString);
//		}
//		
//		String statisticFile = Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-localvariables.properties";
//		try {
//			propertiesStatistics.store(new FileOutputStream(statisticFile),"");
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	
		
		//rename global variables
		/*
		Executor experimentExecutorGlobalVariable = new Executor();
		experimentExecutorGlobalVariable.stGen.setNullInstance();
		experimentExecutorGlobalVariable.stGen.getInstance();
		experimentExecutorGlobalVariable.runRenameRefactoring(ICElement.C_VARIABLE);
		
		//printing statistics
		stGen = experimentExecutorGlobalVariable.stGen;
		mapData = stGen.getInstance().getProgramExecutions();
		logger.info("Program count: " + mapData.keySet().size());
		propertiesStatistics = new Properties();
		for (String programNumber: mapData.keySet()){
			RefactoringData refData = mapData.get(programNumber);
			logger.info("Total refactoring execution time (s): " + refData.getTotalExecutionTimeInSeconds());
			String resultAsString = refData.getResultsAsString();
			logger.info(resultAsString);
			propertiesStatistics.put(programNumber, resultAsString);
		}
		
		statisticFile = Executor.workspacePath + Executor.projectName + Executor.refactoredProgramFolder + "/carefactorResults_rename-globalvariables.properties";
		try {
			propertiesStatistics.store(new FileOutputStream(statisticFile),"");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
*/
		
				
//		Test suite = RenameVariableTests.suite(true); 
//        
//		TestResult tr = new TestResult();
//	    suite.run(tr);
//	    System.out.println("Quantidade de testes falhos: " + tr.failureCount());
	    		
//		Test suiteExtractFunction = ExtractFunctionRefactoringTest.suite();
//		TestResult tr = new TestResult();
//		System.out.println("Quantidade de testes: " + suiteExtractFunction.countTestCases());
//		suiteExtractFunction.run(tr);
//	    System.err.println("Quantidade de testes falhos: " + tr.failureCount());
//		
	    
	    return null;
	}
	
	void outputReferences(String functionName) throws CoreException, InterruptedException { // Access index
		ICProject[] allProjects= CoreModel.getDefault().getCModel().getCProjects(); 
		IIndex index= CCorePlugin.getIndexManager().getIndex(allProjects);
		index.acquireReadLock(); // we need a read-lock on the index 
		try {
			// find bindings for name
			IIndexBinding[] bindings= index.findBindings(functionName.toCharArray(), IndexFilter.ALL_DECLARED, new NullProgressMonitor());
			// find references for each binding
			for (IIndexBinding b : bindings) { 
				if (b instanceof IFunction) {
					outputReferences(index, b);
				}
			} 
		} finally {
			index.releaseReadLock();
		}
	}
		void outputReferences(IIndex index, IBinding b) throws CoreException{ IIndexName[] names= index.findReferences(b); for (IIndexName n : names) {
		outputReference(index, n);
		}
		}
		void outputReference(IIndex index, IIndexName n) throws CoreException { IASTFileLocation fileLoc= n.getFileLocation(); System.out.println(fileLoc.getFileName() + " at offset " + fileLoc.getNodeOffset());
		}
}
