package br.edu.ufcg.ines.carefactor.util;

import java.util.List;

import org.eclipse.cdt.core.model.CModelException;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.IFunction;
import org.eclipse.cdt.core.model.ITranslationUnit;

public class ShowCDTStructures {
	
	public static void main(String[] args) {
		try {
			show("ACExperiments/src/estruturasC.c");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void show(String filePath) throws Exception{
		ITranslationUnit tu = FuncoesGerais.abreArquivo(filePath);
		
		List<ICElement> functions = null;
		List<ICElement> globalVariables = null;
		List<ICElement> localVariables = null;
		try {
			functions = tu.getChildrenOfType(ICElement.C_FUNCTION);
			globalVariables = tu.getChildrenOfType(ICElement.C_VARIABLE);
			localVariables = tu.getChildrenOfType(ICElement.C_VARIABLE_LOCAL);
		} catch (CModelException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("===== Functions =====");
		for (ICElement element:functions){
			System.out.println(element.getElementName());
//			IFunction iFunc = (IFunction)element;
//			System.out.println("Local Variables:");
//			for (ICElement lv: iFunc.getChildrenOfType(ICElement.C_VARIABLE_LOCAL)){
//				System.out.println(element.getElementName());
//			}
		}
		
		System.out.println("\n\n===== Global Variables =====");
		for (ICElement element:globalVariables){
			System.out.println(element.getElementName());
		}

		System.out.println("\n\n===== Local Variables =====");
		for (ICElement element:localVariables){
//			element.getParent().get
			System.out.println(element.getElementName());
		}

	}

}
