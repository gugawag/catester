package br.edu.ufcg.ines.carefactor.util;

import java.io.FileInputStream;
import java.util.Scanner;
import java.util.StringTokenizer;

public class AppRemoveNotCompiledPrograms {
	
	public static void main(String[] args) throws Exception{
		String programsFolder = "/Users/gugawag/Documents/mestrado/runtime-EclipseApplication/ACExperiments/src/experiment1/";
		String propertiesFile = programsFolder + "cdolly.properties";
		
		Scanner scanner = new Scanner(new FileInputStream(propertiesFile));
		
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			System.out.println(line);
			StringTokenizer strToken = new StringTokenizer(line, ",");
			while (strToken.hasMoreTokens()){
				String program = strToken.nextToken();
				FuncoesGerais.executaComando("rm -rf " + programsFolder + program, null);
			}
		}
	}

}
