package br.edu.ufcg.ines.carefactor.util;

public class RegressionTestsResult {
	
	//the tests could not be executed if, for example, some functions has changed their names and the tests expected the old names
	private boolean testsWasExecuted;
	
	//there was compilation and execution of the regression tests, and those passed
	private boolean testsPassed;
	
	private String regressionTestMessage;
	
	

	public RegressionTestsResult(boolean testsWasExecuted, boolean testsPassed,
			String regressionTestMessage) {
		super();
		this.testsWasExecuted = testsWasExecuted;
		this.testsPassed = testsPassed;
		this.regressionTestMessage = regressionTestMessage;
	}
	
	

	public RegressionTestsResult() {
		super();
	}


	public boolean isTestsWasExecuted() {
		return testsWasExecuted;
	}

	public void setTestsWasExecuted(boolean testsWasExecuted) {
		this.testsWasExecuted = testsWasExecuted;
	}

	public boolean isTestsPassed() {
		return testsPassed;
	}

	public void setTestsPassed(boolean testsPassed) {
		this.testsPassed = testsPassed;
	}

	public String getRegressionTestMessage() {
		return regressionTestMessage;
	}

	public void setRegressionTestMessage(String regressionTestMessage) {
		this.regressionTestMessage = regressionTestMessage;
	}
	
	

}
