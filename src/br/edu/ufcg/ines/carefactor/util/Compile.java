package br.edu.ufcg.ines.carefactor.util;
import java.io.File;

import org.apache.log4j.Logger;


public class Compile {

	private static Logger logger = Logger.getLogger(Compile.class);
	


	//compile and run the generated program
	/**
	 * 
	 * @param programPath
	 * @param needAMain If it necessary a file with a main function to compile this program
	 * @return
	 */
	public static boolean run(String programPath, boolean needAMain) {

		FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/script_seq.sh "
							+ programPath, null);
		//copying main.c to be possible compile a program
		if (needAMain){
			FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/main.c "
				+ programPath, null);
		}

		
		File scriptPath = new File(programPath + "/script_seq.sh");
		boolean executouComandoCorretamente = FuncoesGerais.executaComando(scriptPath.getAbsolutePath()
				+ " a.out " + programPath + " " + "c99" + " " + "clang" + " " + " ", null);

		//removing files
		FuncoesGerais.executaComando("rm " + programPath + "/script_seq.sh ", null);
		if (needAMain){
			FuncoesGerais.executaComando("rm " + programPath + "/main.c ", null);
		}
		if (executouComandoCorretamente){
			FuncoesGerais.executaComando("rm " + programPath + "/a.out ", null);
			return true;
		}

        return false;        
	}

}
