package br.edu.ufcg.ines.carefactor.util;

import java.io.File;

public class AppRemoveRegressionTests {
	
	public static void main(String[] args) throws Exception{
		String programsFolder = "/Users/gugawag/Documents/mestrado/runtime-EclipseApplication/ACExperiments/src/experiment1/";
		String propertiesFile = programsFolder + "cdolly.properties";
		
		File programsFolderFile = new File(programsFolder);
		for(File programFolder:programsFolderFile.listFiles()){
			if (programFolder.getName().startsWith(".")){
				continue;
			}
			FuncoesGerais.executaComando("rm -rf " + programFolder.getAbsolutePath() + "/test_regressao.c", null);
		}
	}

}
