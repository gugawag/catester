package br.edu.ufcg.ines.carefactor.model;

/**
 * Represents a result of a refactoring, given the inputs.
 * @author Gustavo Wagner, gugawag@gmail.com
 *
 */
public class RefactoringExpectedResult {
	
	//the new name of the element under refactoring.
	private String newName;
	private boolean sucessRefactoring;
	
	public RefactoringExpectedResult(String newName, boolean sucessRefactoring) {
		super();
		this.newName = newName;
		this.sucessRefactoring = sucessRefactoring;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	/** 
	 * @return True if it is expected that refactoring to the newName be success. False, otherwise.
	 */
	public boolean isSucessRefactoring() {
		return sucessRefactoring;
	}

	public void setSucessRefactoring(boolean sucessRefactoring) {
		this.sucessRefactoring = sucessRefactoring;
	}

}
