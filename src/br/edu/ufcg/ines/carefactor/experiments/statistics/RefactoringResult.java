package br.edu.ufcg.ines.carefactor.experiments.statistics;

import br.edu.ufcg.ines.carefactor.model.RefactoringExpectedResult;


public class RefactoringResult {

	public enum RefactoringType{
		RENAME_FILE, RENAME_FUNCTION, RENAME_LOCAL_VARIABLE, RENAME_GLOBAL_VARIABLE, EXTRACT_LOCAL_VARIABLE, EXTRACT_CONSTANT, EXTRACT_FUNCTION, TOGGLE_FUNCTION;
	}
	
	//Results: 0=fail; 1=success; ...
	//EXCEPTION: exception running the refactoring
	public enum ResultType{
		FAIL, SUCCESS, REFACTORING_INFO, COMPILATION_ERROR, REGRESSION_ERROR, EXCEPTION, REGRESSION_TEST_FAIL, CHANGED_API;
	}
	
	//represents a message if the refactoring fail, for example
	private String message;

	private String result;
	
	private String fileName;
	
	private String oldName;
	private String newName;
	
	private String refactoringType;
	
	private long refatoringExecutionTime;

	private RefactoringExpectedResult expectedResult; 
	
	public long getRefatoringExecutionTime() {
		return refatoringExecutionTime;
	}

	public void setRefatoringExecutionTime(long refatoringExecutionTime) {
		this.refatoringExecutionTime = refatoringExecutionTime;
	}

	public RefactoringResult() {
		this(null,null, null, null, null, null);
	}
	
	public RefactoringResult(ResultType type) {
		this(type,null, null, null, null, null);
	}

	public RefactoringResult(ResultType type, String RefactoringType, String message, String fileName, String oldName, String newName) {
		super();
		if (type != null){
			this.result = type.name();
		}
		this.message = message;
		this.fileName = fileName;
		this.oldName = oldName;
		this.newName = newName;
		this.refactoringType = RefactoringType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}

	public String getNewName() {
		return newName;
	}

	public void setNewName(String newName) {
		this.newName = newName;
	}

	public String getRefactoringType() {
		return refactoringType;
	}

	public void setRefactoringType(String refactoringType) {
		this.refactoringType = refactoringType;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setExpectedResult(RefactoringExpectedResult exceptedResult) {
		this.expectedResult = exceptedResult;
	}
	
	//Result different from expected
	public boolean isResultDifferentExpected(){
		if (expectedResult.isSucessRefactoring() && result.equals(ResultType.SUCCESS)){
			return false;
		}
		return true;
	}
	
	
}
