package br.edu.ufcg.ines.carefactor.experiments.statistics;

import java.util.HashMap;
import java.util.Map;

public class StatisticGenerator {
	private Map<String, RefactoringData> refactoringExecutions;
	
	private static StatisticGenerator instance;
	
	public static StatisticGenerator getInstance(){
		if (instance == null){
			instance = new StatisticGenerator();
		}
		return instance;
	}
	
	public static void setNullInstance(){
		instance = null;
	}

	private StatisticGenerator() {
		super();
		refactoringExecutions = new HashMap<String, RefactoringData>();
	}
	
	public void addRefactoringExecutions(String program){
		refactoringExecutions.put(program, new RefactoringData(program));
	}
	
	public RefactoringData getExecutionDataOfProgram(String program){
		return refactoringExecutions.get(program);
	}

	public Map<String, RefactoringData> getProgramExecutions() {
		return refactoringExecutions;
	}
	

}
